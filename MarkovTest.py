# -*- coding: utf-8 -*-
"""
Created on Tue Sep 20 17:15:07 2016

@author: demerzel
"""

# python
#
# Homework 10 Problem 3
#
# Name: Karthik Mohan
#
#
import random

def createDictionary( filename ):
    """ Input is a string that's the name of the file containing the text. Ouput of this function is a dictionary that generates word combinations based on words encountered in the text file, keys being words in text and value being the word that can follow the said word """
    # '$' is a sentence starting symbol used to represent the words that can be used to start a
    # sentence
    f = open( filename )
    text = f.read()
    f.close()
    LoW = text.split()
    olk = {}
    olk['$'] = [LoW[0]]
    for i in range(len(LoW)-1):
        if LoW[i][-1] == '.' or LoW[i][-1] == '?' or LoW[i][-1] == '!':
            olk['$'] += [LoW[i+1]]
        if LoW[i] not in olk and LoW[i][-1] not in ['.','?','!']:
            olk[LoW[i]] = [LoW[i+1]]
        elif LoW[i] in olk:
            olk[LoW[i]] += [LoW[i+1]]
    return olk

def generateText( d, n ):
    """ This function helps generate a cogent sounding paragraph of n (input, integer) from a dictionary of words (d) """
    words = [random.choice(d['$'])]
    for i in range(n-1):
        words += [random.choice(d.get(words[i],d['$']))]
    senten = ''
    for i in words:
      senten += i + ' '
      continue
    return senten

k = createDictionary('/home/demerzel/pride.txt')
print generateText(k,500)