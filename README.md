# README #
Markov's First Process

* Created this code to test Markov's First Process. It can be employed in various scenarios, like in this case, for text generation.
* Version : The current version works out of the box. Needs a sample body of text and the code should be edited accordingly


### How do I get set up? ###

* Have a sample body of text. Shakespeare's Sonnets might be a good start, haha!
* Edit line 47 (penultimate line) to enter the location of sample text. Edit the number in the last line to change the number of words being generated
* Python 2.7 + Requires random module

### Who do I talk to? ###

* **Karthik Mohan**
* *email : karthik.mohan390@gmail.com*